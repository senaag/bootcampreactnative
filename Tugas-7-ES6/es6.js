var space = " "
//soal no 1
console.log("* No. 1")
console.log(space)

const golden = goldenFunction = () => {
    console.log("this is golden!!")
}

golden()

console.log(space)
//soal no 2
console.log("* No. 2")
console.log(space)

let newFunction = literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)

        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log(space)
//soal no 3
console.log("* No. 3")
console.log(space)

let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject

console.log(firstName, lastName, destination, occupation)

console.log(space)
//soal no 4
console.log("* No. 4")
console.log(space)

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
console.log(combinedArray)

console.log(space)
//soal no 5
console.log("* No. 5")
console.log(space)

const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do 
eiusmod tempor incididunt ut labore et 
dolore magna aliqua. Ut enim ad minim veniam`

console.log(before) 