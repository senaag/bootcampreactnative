var space = " ";
//soal no 1
console.log("* Soal if else");
console.log(space);

var nama = "sena";
var peran = "PenyIHIR";
var pesan = "";
if (nama.trim().length > 0) {

    console.log("Selamat datang di Dunia Werewolf, " + nama);

    if (peran.trim().toLowerCase() == "penyihir") {
        pesan = "Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!";
    } else if (peran.trim().toLowerCase() == "guard") {
        pesan = "Halo Guard " + nama + ", kamu akan melindungi temanmu dalam serangan werewolf.";
    } else if (peran.trim().toLowerCase() == "werewolf") {
        pesan = "Halo Werewolf " + nama + ", kamu akan memangsa tiap malam !";
    } else {
        pesan = "Halo " + nama + ", Pilih peranmu untuk memulai game!";
    }

} else {
    pesan = "Nama harus diisi!";
}
console.log(pesan);
console.log(space);

//soal no 1
console.log("* Soal switch case");
console.log(space);

var hari = 17;
var bulan = 8;
var tahun = 1945;
var namaBulan = "";
switch (bulan) {
    case 1: { namaBulan = "Januari"; break; }
    case 2: { namaBulan = "Februari"; break; }
    case 3: { namaBulan = "Maret"; break; }
    case 4: { namaBulan = "April"; break; }
    case 5: { namaBulan = "Mei"; break; }
    case 6: { namaBulan = "Juni"; break; }
    case 7: { namaBulan = "Juli"; break; }
    case 8: { namaBulan = "Agustus"; break; }
    case 9: { namaBulan = "September"; break; }
    case 10: { namaBulan = "Oktober"; break; }
    case 11: { namaBulan = "November"; break; }
    case 12: { namaBulan = "Desember"; break; }
    default: { }
}

if (namaBulan.trim().length > 0 &&
    hari > 0 &&
    hari < 32 &&
    bulan > 0 &&
    bulan < 13 &&
    tahun > 1900 &&
    tahun < 2999) {
    console.log(hari + " " + namaBulan + " " + tahun);
} else {
    console.log("pengisian data salah");
}