var space = " "
//soal no 1
console.log("* No. 1")
console.log(space)

function teriak(){
    return "Halo Sanbers!"
}

console.log(teriak())

console.log(space)
console.log("* No. 2")
console.log(space)

function kalikan(angkaPertama, angkaKedua){
    return angkaPertama * angkaKedua
}

var num1 = 12
var num2 = 4

var hasilkali = kalikan(12,4)

console.log(hasilkali)

console.log(space)
console.log("* No. 3")
console.log(space)

function introduce(nama, age, address, hobby){
    return "Nama saya "+ nama +", umur saya "+ age +" tahun, alamat saya di "+ address +" dan saya mempunyai hobby "+ hobby
}

var nami = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(nami, age, address, hobby)
console.log(perkenalan)