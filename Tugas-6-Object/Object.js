var space = " "
//soal no 1
console.log("* No. 1")
console.log(space)

function arrayToObject(arr) {
    var tinggi = arr.length
    var m = 0
    var age = 0
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    while (m < tinggi) {
        var nomer = m + 1
        var namaobject = arr[m][0] + " " + arr[m][1]
        var namaobject = {}

        if (arr[m][3] == null || arr[m][3] == "" || arr[m][3] >= thisYear) {
            age = "Invalid Birth Year"
        } else {
            age = thisYear - arr[m][3]
        }

        namaobject.firstName = arr[m][0]
        namaobject.lastName = arr[m][1]
        namaobject.gender = arr[m][2]
        namaobject.age = age

        function liatobject(obj) {
            return obj
        }
        console.log(nomer + ". " + namaobject.firstName + " " + namaobject.lastName)
        console.log(liatobject(namaobject))
        m++
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

arrayToObject([])


//soal no 2
console.log(space)
console.log("* No. 2")
console.log(space)

function shoppingTime(memberId, money) {
    var pesan = ""
    if (memberId == "" || memberId == null || memberId > 0) {
        pesan = "Mohon maaf, toko X hanya berlaku untuk member saja"

    } else {
        if (money < 50000) {
            pesan = "Mohon maaf, uang tidak cukup"

        } else {
            var objectId = memberId
            var objectId = {}
            objectId.memberId = memberId
            objectId.money = money


            var barang = [

                [500000, "Baju Zoro"],
                [250000, "Baju H&N"],
                [175000, "Sweater Uniklooh"],
                [50000, "Casing Handphone"],
                [1500000, "Sepatu Stacattu"],

            ]
            barang.sort(function (a, b) {
                return b[0] - a[0]
            })
            var barangbelanja = []
            var totalbelanja = 0
            var tinggi = barang.length
            var i = 0
            while (i < tinggi) {
                if (money - totalbelanja >= barang[i][0]) {
                    totalbelanja += barang[i][0]
                    barangbelanja.push(barang[i][1])
                }
                i++
            }
            objectId.listPurchased = barangbelanja

            var kembalian = money - totalbelanja
            objectId.changeMoney = kembalian
            pesan = objectId
        }
    }
    return pesan
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));

console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

//soal no 3
console.log(space)
console.log("* No. 3")
console.log(space)

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    function indexOf(huruf) {
        var jml = rute.length
        for (var j = 0; j < jml; j++) {
            if (rute[j] == huruf || j == jml - 1) {
                return j
            }

        }
    }

    var data = []
    var tinggi = arrPenumpang.length
    var i = 0
    var nama = ""
    var asal = ""
    var tujuan = ""
    var ongkos = 0
    while (i < tinggi) {
        nama = arrPenumpang[i][0]
        asal = arrPenumpang[i][1]
        tujuan = arrPenumpang[i][2]
        ongkos = (indexOf(tujuan) - indexOf(asal)) * 2000
        if (ongkos < 0) { ongkos = ongkos * -1 }

        var namaobject = nama
        var namaobject = {}
        namaobject.penumpang = nama
        namaobject.naikDari = asal
        namaobject.tujuan = tujuan
        namaobject.bayar = ongkos
        data.push(namaobject)
        i++

    }
    return data
}


console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B'], ['sena', 'E', 'A']]));

console.log(naikAngkot([])); //[]