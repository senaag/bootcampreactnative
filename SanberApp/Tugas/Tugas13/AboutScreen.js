import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native'
import { AntDesign, Ionicons, FontAwesome5 } from '@expo/vector-icons'

export default function AboutScreen() {


    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{ color: 'white' }}>.</Text>
                    <TouchableOpacity>
                    <AntDesign name="logout" size={24} color="black" />
                    </TouchableOpacity>
                </View>
                <Text style={styles.textHeader}>Tentang Saya</Text>
                <Image
                    style={{ height: 200, width: 200, borderRadius: 100, margin: 5 }}
                    source={require('./asset/senafoto.jpg')}
                />
                <Text style={styles.textHeader}>Sena Achmad Gumilar</Text>
                <Text style={styles.textHeader}>Freelancer Developer</Text>
                <View style={styles.boxDeskripsi}>
                    <Text style={{ marginLeft: 5 }}>Portofolio</Text>
                    <View style={{ width: 300, borderBottomWidth: 1, borderBottomColor: '#A8AAAB' }} />
                    <View style={styles.boxItems}>
                        <TouchableOpacity style={styles.itemIcon}>
                            <AntDesign name="gitlab" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>@senaag

                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemIcon}>
                            <AntDesign name="github" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>@senaag

                        </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.boxDeskripsi}>
                    <Text style={{ marginLeft: 5 }}>Contact</Text>
                    <View style={{ width: 300, borderBottomWidth: 1, borderBottomColor: '#A8AAAB' }} />
                    <View style={styles.boxItems}>
                        <TouchableOpacity style={styles.itemIcon}>
                            <AntDesign name="linkedin-square" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>sena ahmad

                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemIcon}>
                            <Ionicons name="logo-whatsapp" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>081321795777

                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemIcon}>
                            <AntDesign name="mail" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>senna46

                        </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.boxDeskripsi}>
                    <Text style={{ marginLeft: 5 }}>Technology</Text>
                    <View style={{ width: 300, borderBottomWidth: 1, borderBottomColor: '#A8AAAB' }} />
                    <View style={styles.boxItems}>
                        <TouchableOpacity style={styles.itemIcon}>
                            <FontAwesome5 name="php" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>intermedian

                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemIcon}>
                            <FontAwesome5 name="java" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>intermedian

                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.itemIcon}>
                            <FontAwesome5 name="react" size={48} color="black" />
                            <Text style={{ fontSize: 18 }}>basic

                        </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.boxDeskripsi}>
                    <Text style={{ marginLeft: 5 }}>Skill</Text>
                    <View style={{ width: 300, borderBottomWidth: 1, borderBottomColor: '#A8AAAB' }} />
                    <Text style={{ marginTop: 5, marginLeft: 5 }}>Desktop Developer</Text>
                    <View style={{ height: 15, width: 180, backgroundColor: "cyan", borderTopRightRadius: 5, borderBottomRightRadius: 5, margin: 5 }} />
                    <Text style={{ marginTop: 5, marginLeft: 5 }}>Web Developer</Text>
                    <View style={{ height: 15, width: 260, backgroundColor: "green", borderTopRightRadius: 5, borderBottomRightRadius: 5, margin: 5 }} />
                    <Text style={{ marginTop: 5, marginLeft: 5 }}>Mobile Developer</Text>
                    <View style={{ height: 15, width: 120, backgroundColor: "yellow", borderTopRightRadius: 5, borderBottomRightRadius: 5, margin: 5 }} />
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    header: {
        height: 50,
        width: 300,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between'
    },
    textHeader: {
        fontSize: 24,
        color: '#003366',
        marginBottom: 5
    },
    boxDeskripsi: {
        backgroundColor: '#EFEFEF',
        justifyContent: 'flex-start',
        marginBottom: 10
    },
    boxItems: {
        flexDirection: 'row',
        height: 100,
        alignItems: 'baseline',
        justifyContent: 'space-around',
        marginBottom: 10
    },
    itemIcon: {
        alignItems: 'center',
        paddingTop: 10
    }
})
