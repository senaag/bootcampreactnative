import React from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView } from 'react-native'


export default function LoginScreen() {


    return (
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.header}>
                <Image
                    source={require('./asset/logosanbercode.png')}
                />
            </View>
            <Text style={styles.formTitle}>Login</Text>
            <View style={styles.form}>

                <Text style={{ marginLeft: 5 }}>username/email</Text>
                <TextInput style={styles.inputText}
                />
                <Text style={{ marginLeft: 5 }}>password</Text>
                <TextInput style={styles.inputText} secureTextEntry={true}
                />
                
            </View>
            <View style={{ alignItems: 'center', marginTop: 20 }}>
                <TouchableOpacity>
                    <View style={styles.buttonDark}>
                        <Text style={{ fontSize: 18, color: 'white' }}>DAFTAR</Text>
                    </View>
                </TouchableOpacity>
                <Text style={{ fontSize: 18, marginBottom: 5 }}>atau</Text>
                <TouchableOpacity>
                    <View style={styles.buttonLight}>
                        <Text style={{ fontSize: 18, color: '#3EC6FF' }}>MASUK</Text>
                    </View>
                </TouchableOpacity>
            </View>

        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    header: {
        marginTop: 30,
        justifyContent: 'center'
    },
    form: {
        //flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
    },
    formTitle: {
        color: '#003366',
        fontSize: 18,
        fontWeight: 'bold',
        paddingBottom: 5,
        marginTop: 10,
        marginBottom: 10
    },
    formInput: {
        backgroundColor: 'white',
        justifyContent: 'flex-start'
    },
    inputText: {
        height: 40,
        width: 300,
        borderColor: '#003366',
        borderWidth: 1,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: 5,
        borderRadius: 10
    },
    buttonDark: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: '#003366',
        borderRadius: 20,
        marginBottom: 10
    },
    buttonLight: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#3EC6FF',
        borderRadius: 20,
        marginBottom: 10
    }
})
