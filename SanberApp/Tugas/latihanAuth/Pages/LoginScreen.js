import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView, Alert } from 'react-native'
import * as firebase from 'firebase'

export default function LoginScreen({ navigation }) {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const firebaseConfig = {
        apiKey: "AIzaSyAbpmSFf464EHV_11sUIN3ETaRG0cK7-eY",
        authDomain: "sanbercodeapp-538b0.firebaseapp.com",
        projectId: "sanbercodeapp-538b0",
        storageBucket: "sanbercodeapp-538b0.appspot.com",
        messagingSenderId: "910230241881",
        appId: "1:910230241881:web:f68b5a2006611f8d0f6748",
        measurementId: "G-1XH71BX07B"
    };
    // Initialize Firebase
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
        //firebase.analytics();
    }

    const submit = () => {

        if (email == "" || password == "") {
            Alert.alert(
                "Data belum lengkap",
                "Isi dahulu data anda dengan lengkap",
                [
                    { text: "OK", onPress: () => console.log("data belum lengkap") }
                ],
                { cancelable: false }
            );
        } else {
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(() => {
                    console.log('User account signed in!');
                    navigation.navigate("Beranda");
                })
                .catch(error => {

                    console.error(error);
                });
        }
    }
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        source={require('../asset/logosanbercode.png')}
                    />
                </View>
                <Text style={styles.formTitle}>Login</Text>
                <View style={styles.form}>

                    <Text style={{ marginLeft: 5 }}>email</Text>
                    <TextInput
                        style={styles.inputText}
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />
                    <Text style={{ marginLeft: 5 }}>password</Text>
                    <TextInput
                        style={styles.inputText}
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />

                </View>
                <View style={{ alignItems: 'center', marginTop: 20 }}>

                    <TouchableOpacity
                        onPress={submit}
                    >
                        <View style={styles.buttonLight}>
                            <Text style={{ fontSize: 18, color: '#3EC6FF' }}>MASUK</Text>
                        </View>
                    </TouchableOpacity>
                    <Text style={{ fontSize: 18, marginBottom: 5 }}>atau</Text>
                    <TouchableOpacity
                        onPress={() => navigation.navigate("Register")}
                    >
                        <View style={styles.buttonDark}>
                            <Text style={{ fontSize: 18, color: 'white' }}>DAFTAR</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    header: {
        marginTop: 30,
        justifyContent: 'center'
    },
    form: {
        //flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
    },
    formTitle: {
        color: '#003366',
        fontSize: 18,
        fontWeight: 'bold',
        paddingBottom: 5,
        marginTop: 10,
        marginBottom: 10
    },
    formInput: {
        backgroundColor: 'white',
        justifyContent: 'flex-start'
    },
    inputText: {
        height: 40,
        width: 300,
        borderColor: '#003366',
        borderWidth: 1,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        marginBottom: 5,
        borderRadius: 10
    },
    buttonDark: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: '#003366',
        borderRadius: 20,
        marginBottom: 10
    },
    buttonLight: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#3EC6FF',
        borderRadius: 20,
        marginBottom: 10
    }
})
