import React, { useState } from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import * as firebase from 'firebase'

export default function Home({ navigation }) {

    const logout = () => {
        firebase.auth().signOut()
            .then(() => {
                console.log("user berhasil logout");
                navigation.navigate("Login Screen");
            })
    }
    return (
        <View style={styles.container}>
            <Text>Home screen</Text>
            <Button
                title="Logout"
                onPress={logout}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
