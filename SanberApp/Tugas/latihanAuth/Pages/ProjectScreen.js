import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Button, Alert } from 'react-native'
import * as firebase from 'firebase'

export default function ProjectScreen({ navigation }) {
    const [user, setUser] = useState("")

    useEffect(() => {
        const userInfo = firebase.auth().currentUser
        setUser(userInfo)
    }, [])

    const logout = () => {
        Alert.alert(
            "Logout",
            "Yakin anda ingin keluar?",
            [
                { text: "Cancel", onPress: () => console.log("batal") },
                {
                    text: "OK", onPress: () => {
                        firebase.auth().signOut()
                            .then(() => {
                                console.log("user berhasil logout");
                                navigation.navigate("Login Screen");
                            })
                    }
                }
            ],
            { cancelable: false }
        );

    }

    return (
        <View style={styles.container}>
            <Text>project screen</Text>
            <Text>Haloo, {user.email}</Text>
            <Button
                title="Logout"
                onPress={logout}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
