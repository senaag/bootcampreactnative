import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import HomeScreen from "../Pages/Home";
import AboutScreen from "../Pages/AboutScreen";
import LoginScreen from "../Pages/Login";
import ProjectScreen from "../Pages/ProjectScreen";
import SettingScreen from "../Pages/Setting";
import SkillScreen from "../Pages/SkillProject";
import AddScreen from "../Pages/AddScreen";

import Ionicons from '@expo/vector-icons/Ionicons';

const Tab = createBottomTabNavigator()
const Drawwer = createDrawerNavigator()
const Stack = createStackNavigator()

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login Screen" component={LoginScreen} />
                <Stack.Screen name="Home Screen" component={HomeScreen} />
                <Stack.Screen name="Main App" component={MainApp} />
                <Stack.Screen name="Beranda" component={MyDrawwer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp = () => (
    <Tab.Navigator
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === 'Project Screen') {
                    iconName = focused
                        ? 'briefcase'
                        : 'briefcase-outline';
                } else if (route.name === 'Add Screen') {
                    iconName = focused ? 'add-circle' : 'add-circle-outline';
                } else if (route.name === 'Skill Screen') {
                    iconName = focused ? 'construct' : 'construct-outline';
                }

                // You can return any component that you like here!
                return <Ionicons name={iconName} size={size} color={color} />;
            },
        })}
        tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
        }}
    >
        <Tab.Screen name="Project Screen" component={ProjectScreen} />
        <Tab.Screen name="Add Screen" component={AddScreen} />
        <Tab.Screen name="Skill Screen" component={SkillScreen} />
    </Tab.Navigator>
)

const MyDrawwer = () => (
    <Drawwer.Navigator>
        <Drawwer.Screen name="App" component={MainApp} />
        <Drawwer.Screen name="About Screen" component={AboutScreen} />
    </Drawwer.Navigator>
)