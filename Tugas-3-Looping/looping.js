var space = " ";
//soal no 1
console.log("* No. 1 Looping While");
console.log(space);

var i = 1;
console.log("LOOPING PERTAMA");
while (i < 20) {
    i++;
    console.log(i + " - I Love Coding");
}
console.log("LOOPING KEDUA");
while (i > 2) {
    i--;
    console.log(i + " - I will become a mobile developer");
}

console.log(space);
console.log("* No. 2 Looping menggunakan for");
console.log(space);

for (var j = 1; j <= 20; j++) {
    if (j % 3 == 0) {
        console.log(j + " - I Love Coding");
    } else {
        if (j % 2 == 0) {
            console.log(j + " - Berkualitas");
        } else {
            console.log(j + " - Santai");
        }
    }
}

console.log(space);
console.log("* 3 Membuat Persegi Panjang #");
console.log(space);

for (var l = 1; l <= 4; l++) {
    var pagar = "#";
    for (var k = 1; k < 8; k++) {
        pagar = pagar + "#";

    }
    console.log(pagar);
}

console.log(space);
console.log("* 4 Membuat Tangga");
console.log(space);

var pagar = "#";
for (var k = 1; k < 8; k++) {
    console.log(pagar);
    pagar = pagar + "#";

}

console.log(space);
console.log("* 5 Membuat Papan Catur");
console.log(space);

for (var l = 1; l <= 8; l++) {
    var pagar = "#";
    if (l % 2 == 0) {
        for (var k = 1; k < 8; k++) {
            pagar = pagar + " #";

        }
    } else {
        pagar = " "+pagar;
        for (var k = 1; k < 8; k++) {
            pagar = pagar + " #";

        }
    }
    console.log(pagar);
}