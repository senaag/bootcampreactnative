var space = " "
//soal no 1
console.log("* No. 1")
console.log(space)

class Animal {
    constructor(nama) {
        this.name = nama
        this.legs = 4
        this.cold_blooded = false
    }

}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal {
    constructor(nama) {
        super(nama)
        this.legs = 2
    }
    yell() {
        return console.log('Auooo')
    }
}

class Frog extends Animal {
    constructor(nama) {
        super(nama)
        this.cold_blooded = true
    }
    jump() {
        return console.log('Hop Hop')
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)


var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

//soal no 2
console.log(space)
console.log("* No. 2")
console.log(space)

class Clock {
    constructor(template,durasi) {
        this.tmplt = template.template
        this.timer
        this.durasi = durasi


    }

    render() {
        let date = new Date()

        let hours = date.getHours()
        if (hours < 10) { hours = '0' + hours }

        let mins = date.getMinutes()
        if (mins < 10) { mins = '0' + mins }

        let secs = date.getSeconds()
        if (secs < 10) { secs = '0' + secs }

        let output = this.tmplt
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs)



        console.log(output)
    }

    stop() {
        clearInterval(this.timer)
    }
    start() {
        let i = 0
        
        this.timer = setInterval(() => {

            this.render()
            i = i + 1000
            if (i == this.durasi) {
                this.stop() // jika sudah sesuai durasi maka program berhenti
            }

        }, 1000);
    }
}

let clock = new Clock({ template: 'h:m:s' },10000); // ditambah variabel durasi
clock.start()