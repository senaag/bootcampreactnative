var space = " "
//soal no 1
console.log("* No. 1")
console.log(space)



function range(startNum, finishNum) {
    var nosatu = []
    var i = 0
    if (finishNum == "" || finishNum == null || startNum == "" || startNum == null) {
        nosatu.push(-1)
    } else {
        if (startNum <= finishNum) {
            for (i = startNum; i <= finishNum; i++) {
                nosatu.push(i)
            }
        } else {
            for (i = startNum; i >= finishNum; i--) {
                nosatu.push(i)
            }
        }
    }
    return nosatu
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log(space)
console.log("* No. 2")
console.log(space)

function rangeWithStep(startNum, finishNum, step) {
    var nodua = []
    var j = 0
    if (finishNum == "" || finishNum == null || startNum == "" || startNum == null) {
        nodua.push(-1)

    } else if (step == "" || step == null) {
        if (startNum <= finishNum) {
            j = startNum
            while (j <= finishNum) {
                nodua.push(j)
                j++
            }

        } else {
            j = startNum
            while (j >= finishNum) {
                nodua.push(j)
                j--
            }
        }
    } else {
        if (startNum <= finishNum) {
            j = startNum
            while (j <= finishNum) {
                nodua.push(j)
                j = j + step
            }

        } else {
            j = startNum
            while (j >= finishNum) {
                nodua.push(j)
                j = j - step
            }
        }
    }
    return nodua
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(space)
console.log("* No. 3")
console.log(space)


function sum(startNum, finishNum, step) {

    var array3 = rangeWithStep(startNum, finishNum, step)
    var jumlah = 0
    var l = array3.length
    if (finishNum == "" || finishNum == null || step == "" || step == null) {
        if (startNum == null || startNum == "") { startNum = 0 }
        return startNum
    } else {
        for (var k = 0; k < l; k++) {
            jumlah = jumlah + array3[k]
        }
    }
    return jumlah

}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log(space)
console.log("* No. 4")
console.log(space)

function dataHandling(arrayy) {
    var tinggi = arrayy.length
    var m = 0
    while (m < tinggi) {
        console.log("Nomor ID : " + arrayy[m][0])
        console.log("Nama Lengkap : " + arrayy[m][1])
        console.log("TTL : " + arrayy[m][2] + " " + arrayy[m][3])
        console.log("Hobi : " + arrayy[m][4])
        console.log("")
        m++
    }


}


var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

console.log(space)
console.log("* No. 5")
console.log(space)

function balikKata(kata) {
    var panjang = kata.length
    var katabalik = ""
    while (panjang >= 1) {
        panjang--
        katabalik = katabalik + kata[panjang]

    }
    return katabalik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log(space)
console.log("* No. 6")
console.log(space)

function dataHandling2(arrayy) {

    var namabaru = arrayy[1] + "Elsharawy"
    var provinsibaru = "Propinsi " + arrayy[2]
    arrayy.splice(1, 2, namabaru, provinsibaru)
    arrayy.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(arrayy)

    var tanggal = arrayy[3]
    var tanggalaray = tanggal.split("/")
    var namaBulan = ""
    var bln = Number(tanggalaray[1])
    switch (bln) {
        case 1: { namaBulan = "Januari"; break; }
        case 2: { namaBulan = "Februari"; break; }
        case 3: { namaBulan = "Maret"; break; }
        case 4: { namaBulan = "April"; break; }
        case 5: { namaBulan = "Mei"; break; }
        case 6: { namaBulan = "Juni"; break; }
        case 7: { namaBulan = "Juli"; break; }
        case 8: { namaBulan = "Agustus"; break; }
        case 9: { namaBulan = "September"; break; }
        case 10: { namaBulan = "Oktober"; break; }
        case 11: { namaBulan = "November"; break; }
        case 12: { namaBulan = "Desember"; break; }
        default: { }
    }
    console.log(namaBulan)

    var tanggalbalik = []
    tanggalbalik.push(tanggalaray[2])
    tanggalbalik.push(tanggalaray[0])
    tanggalbalik.push(tanggalaray[1])
    console.log(tanggalbalik)

    var tanggalbaru = tanggalaray.join("-")
    console.log(tanggalbaru)

    var namaarray = arrayy[1].split(" ")
    var duakatanama = namaarray[0] + " " + namaarray[1]
    console.log(duakatanama)


}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)