let readBooksPromise = require('./promise.js')

let books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let waktubaca = 10000
let i = 0

function bacaBuku(baca, i) {
    readBooksPromise(baca, books[i])
        .then(function (fulfilled) {

            if (i < books.length - 1) {
                bacaBuku(fulfilled, i + 1)
            } else if (i == books.length - 1) {
                console.log("semua buku sudah dibaca")
            } else {
                bacaBuku(fulfilled, i)
            }
        })
        .catch(function (error) {

            console.log(error.message);

        });
}

// Tanya Mom untuk menagih janji
bacaBuku(waktubaca, i) 